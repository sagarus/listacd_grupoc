/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colecciones;

/**
 *
 * @author JUAN PABLO
 */
public class Cola<T> {
    
  private  NodoD<T> cabeza;
  private  NodoD<T> front;
    private int tamanio;
    
    public Cola() {
        
        this.cabeza=new NodoD();
        this.cabeza.setSig(cabeza);
        this.cabeza.setAnt(cabeza);
        //sobra:
        this.cabeza.setInfo(null);
        
        
    }
    
   public void add(T info){
    this.front=new NodoD<T>(info, this.cabeza.getSig(),this.cabeza);
    //redireccionar
    this.cabeza.getSig().setAnt(this.front);
    this.cabeza.setSig(this.front);
    
    this.tamanio++;
    
   
    }
    
    public boolean esVacio()
    {
        
    return (this.cabeza==this.cabeza.getSig());
    //return (this.cabeza==this.cabeza.getAnt());
    //return this.tamanio==0;
    }
    public void remove(){
    NodoD<T> eliminar=this.front;
    this.front.getAnt().setSig(this.front.getSig());
    this.front.getSig().setAnt(this.front.getAnt());
    this.front=this.front.getAnt();
    eliminar.setAnt(null);
    eliminar.setSig(null);
    eliminar.setInfo(null);
    
    }

    public NodoD<T> getCabeza() {
        return cabeza;
    }

    public void setCabeza(NodoD<T> cabeza) {
        this.cabeza = cabeza;
    }

    public NodoD<T> getFront() {
        return front;
    }

    public void setFront(NodoD<T> front) {
        this.front = front;
    }

    public int getTamanio() {
        return tamanio;
    }

    public void setTamanio(int tamanio) {
        this.tamanio = tamanio;
    }
}


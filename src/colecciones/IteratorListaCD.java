/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colecciones;

import java.util.Iterator;

/**
 *
 * @author estudiante
 */
public class IteratorListaCD<T> implements Iterator<T> {

    
    private NodoD<T> cabeza,sig ;
     
    public IteratorListaCD(NodoD<T> cabeza) {
        //Lo dejo quieto
        this.cabeza = cabeza;
        //Es el que voy a mover
        this.sig=this.cabeza.getSig();
    }
    
    
    
    
    @Override
    public boolean hasNext() {
        return this.sig!=this.cabeza;
    }

    @Override
    public T next() {
        if(this.hasNext())
        {
            this.sig=this.sig.getSig();
            return this.sig.getAnt().getInfo();
        }
     return null;
    }
 @Override
    public void remove() {
        sig.getAnt().setSig(sig.getSig());
        sig.getSig().setAnt(sig.getAnt());
        NodoD<T> eliminar=sig;
        sig=sig.getSig();
        eliminar.setAnt(null);
        eliminar.setSig(null);
        eliminar.setInfo(null);
        
   }
}
     
    
       
 
    


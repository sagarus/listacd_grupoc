/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colecciones;

/**
 *
 * @author JUAN PABLO
 * @param <T>
 */
public class Pila<T>    {
    
    private  NodoD<T> cabeza;
    private  NodoD<T> tail;
    private int tamanio;
    
    public Pila() {
        
        this.cabeza=new NodoD();
        this.cabeza.setSig(cabeza);
        this.cabeza.setAnt(cabeza);
        //sobra:
        this.cabeza.setInfo(null);
        
        
    }
    
    public void push(T info)
    { this.tail=new NodoD<T>(info, this.cabeza,this.cabeza.getAnt());
    this.cabeza.setAnt(tail);
    tail.getAnt().setSig(tail);
    this.tamanio++;
    
    }
    
   public boolean organizado(Cola<String> cola)           
    { 
        Cola<String>duplicado=cola;
    
    Pila<Integer> pila=new Pila();
    while(cola.getTamanio()!=0)
    {
        switch(duplicado.getFront().getInfo())
        {
            case "{": pila.push(2); break;
            case "[":pila.push(3);  break;
            case "(":pila.push(1);  break;
            default : return false;      
        }
    duplicado.remove(); 
    if(duplicado.getFront().getInfo().equals("}")||
            duplicado.getFront().getInfo().equals("]")||
            duplicado.getFront().getInfo().equals(")"))
        pila.Pop();
    }
    return true;
    
    }
    public NodoD<T> getCabeza() {
        return cabeza;
    }

    public void setCabeza(NodoD<T> cabeza) {
        this.cabeza = cabeza;
    }

    public NodoD<T> getTail() {
        return tail;
    }

    public void setTail(NodoD<T> tail) {
        this.tail = tail;
    }

    public int getTamanio() {
        return tamanio;
    }

    public void setTamanio(int tamanio) {
        this.tamanio = tamanio;
    }
    
    public boolean esVacio()
    {
        
    return (this.cabeza==this.cabeza.getSig());
    //return (this.cabeza==this.cabeza.getAnt());
    //return this.tamanio==0;
    }
    
    public void Pop(){
    NodoD<T> eliminar=this.tail;
    this.tail.getAnt().setSig(this.tail.getSig());
    this.tail.getSig().setAnt(this.tail.getAnt());
    this.tail=this.tail.getAnt();
    eliminar.setAnt(null);
    eliminar.setSig(null);
    eliminar.setInfo(null);
    }
}

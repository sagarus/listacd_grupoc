/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colecciones;

import java.util.Iterator;

/**
 *
 * @author estudiante
 * @param <T>
 */
public class ListaCD<T> implements Iterable<T>{
    
    private  NodoD<T> cabeza;
    private int tamanio;

    public ListaCD() {
        
        this.cabeza=new NodoD();
        this.cabeza.setSig(cabeza);
        this.cabeza.setAnt(cabeza);
        //sobra:
        this.cabeza.setInfo(null);
        
        
    }

    
    public void insertarInicio(T info)
    {
    NodoD<T> nuevo=new NodoD<T>(info, this.cabeza.getSig(),this.cabeza);
    //redireccionar
    this.cabeza.getSig().setAnt(nuevo);
    this.cabeza.setSig(nuevo);
    
    this.tamanio++;
    }
    
    //O(1)
    public void insertarFinal(T info)
    {
    NodoD<T> nuevo=new NodoD<T>(info, this.cabeza,this.cabeza.getAnt());
    this.cabeza.setAnt(nuevo);
    nuevo.getAnt().setSig(nuevo);
    this.tamanio++;
    }
         
    
    public String toString()
    {
    String msg="";
    for(NodoD<T> x=this.cabeza.getSig();x!=this.cabeza;x=x.getSig())
        msg+=x.getInfo()+"<->";
    return msg;
    }
    
    
    public boolean esVacio()
    {
        
    return (this.cabeza==this.cabeza.getSig());
    //return (this.cabeza==this.cabeza.getAnt());
    //return this.tamanio==0;
    }
    
    
    public int getTamanio() {
        return tamanio;
    }
    
    
    public T get(int i)
    {
        try{
            return this.getPos(i).getInfo();
        }catch(Exception e)
        {
        System.err.println(e.getMessage());
        return null;
        }
    
    }
    
    
    public void set(int i, T info)
    {
     try{
         this.getPos(i).setInfo(info);
        }catch(Exception e)
        {
        System.err.println(e.getMessage());
        
        }
    }
    
    
    
    
    private NodoD<T> getPos(int i) throws Exception
    {
    if(this.esVacio() || i<0 || i>=this.tamanio)
        throw new Exception("El índice esta fuera de rango de la lista circular doble");
    
    NodoD<T> x=this.cabeza.getSig();
    
    while(i>0)
    {
        x=x.getSig();
        i--;
    }
    return x;
    }

    @Override
    public Iterator<T> iterator() {
        return new IteratorListaCD(this.cabeza);
    }
    
    public void Concatenar(ListaCD l2,int posicion){
     if(this.tamanio<1||posicion>this.tamanio||l2.tamanio<1){   
         System.out.print("una lista esta vacia o la posicion a concatenar no existe");
         return;
     }
     try{   
      NodoD<T> pos=  this.getPos(posicion);
      l2.cabeza.getAnt().setSig(pos.getSig());
    l2.cabeza.getSig().setAnt(pos);
    pos.getSig().setAnt(l2.cabeza.getAnt());
    pos.setSig(l2.cabeza.getSig());
    
     }
     catch(Exception e){
         System.err.println(e.getMessage());
    }
     tamanio+=l2.getTamanio();
    }
  public void removeD() {
      
        NodoD <T> check=this.cabeza.getSig();
        NodoD <T> sig=check.getSig();
        while(check!=cabeza){
       
            if(sig==cabeza)    {    
        check=check.getSig();
        sig=check.getSig(); 
        
        }
            
            if(check.getInfo()==(sig.getInfo())) 
       {
        sig.getAnt().setSig(sig.getSig());
        sig.getSig().setAnt(sig.getAnt());
        NodoD<T> eliminar=sig;
        sig=sig.getSig();
        eliminar.setAnt(null);
        eliminar.setSig(null);
        eliminar.setInfo(null);
        tamanio--;
      }
       else
       sig=sig.getSig();
       }
        
  
       
       
     }
    }
   

